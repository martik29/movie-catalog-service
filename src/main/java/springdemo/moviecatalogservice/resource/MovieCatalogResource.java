package springdemo.moviecatalogservice.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import springdemo.moviecatalogservice.models.CatalogItem;
import springdemo.moviecatalogservice.models.Movie;
import springdemo.moviecatalogservice.models.Rating;
import springdemo.moviecatalogservice.models.UserRating;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/catalog")
public class MovieCatalogResource {

    @Autowired
    public RestTemplate restTemplate;
    @RequestMapping("/{userId}")
    public List<CatalogItem> getCatalog(@PathVariable("userId") String userId){

        //get all listed movie ids.
        /*
           List<Rating> ratings = Arrays.asList(
                new Rating("1234", 4),
                new Rating("5678", 3)
        );
        */

        UserRating userRating = restTemplate.getForObject("http://localhost:8082/rating/users/" + userId, UserRating.class);
        return userRating.getGetRatings().stream().map(rating -> {
            Movie movie = restTemplate.getForObject("http://localhost:8081/movie/" + rating.getMovieId(), Movie.class);
            return new CatalogItem(movie.getMovieName(), "test", rating.getRating());
        }).collect(Collectors.toList());

        //new CatalogItem("filhal 1:", "about real", 5)));
        //for each movie id call movie info
        //put together in single unit
        //return Collections.singletonList(new CatalogItem("filhal 1:", "about real", 5));
    }
}
